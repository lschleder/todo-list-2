# Todo List #

This app creates a place to keep track of tasks.

### Installation ###

* npm install
* npm run dev
* composer install
* go to local host to see app working

### Todo List ###

- Adds items to list using form input
- Display list of completed items and remaining items
- Marks item as complete (with visual indication)
    - Move to bottom 
    - Change color
- You can remove items from list
- double click remaining item to edit it

### Resources ###

* ![Screenshot](screenshot.png) 