require('./bootstrap');

import Vue from 'vue'

//Main pages
import App from './views/list.vue'


const app = new Vue({
    el: '#tasks',
    components: { App }
});